module.exports = {
    "extends": [
        "airbnb",
        "plugin:meteor/recommended"
    ],
    "plugins": [
        "react",
        "meteor"
    ],
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true,
            "experimentalObjectRestSpread": true
        }
    },
    "env": {
        "meteor": true,
        "node": true,
        "browser": true
    },
    "rules": {
        "semi": [2, "never"],
        "no-unexpected-multiline": 2,
        "no-throw-literal": 0,
        "arrow-body-style": [2, "always"],
        "quotes": [2, "single", {"allowTemplateLiterals": true}],
        "max-len": [2, 150],
        "meteor/no-session": [0],
        "meteor/prefer-session-equals": [2],
        "import/no-unresolved": [2, { ignore: ['meteor/*'] }]
    },
    "globals": {
        "Foo": false,
        "Bar": false
    }
};