import twilio from 'twilio'
import moment from 'moment'

// set up variables
const accountSid = Meteor.settings.private.twilio.account_sid
const authToken = Meteor.settings.private.twilio.auth_token
const client = twilio(accountSid, authToken)

export default function () {
  Meteor.methods({
    'getMessages'() {
      const getMessages = Meteor.wrapAsync(client.messages.list)
      const data = getMessages()

      // Make sure the user is logged in before inserting a task
      if (! this.userId) {
        throw new Meteor.Error('not-authorized')
      }

      // check if there are messages
      if (data.messages.length === 0) {
        return data.messages
      }

      const filtered = data.messages.filter((message) => {
        if (message.from === Meteor.settings.public.twilio.number || message.to === Meteor.settings.public.twilio.number) {
          return message
        }
      })
      
      const messages = filtered.map((message) => {
        // correct date format
        // must be in the form '13 06 2016 13:01'
        const dateObj = new Date(message.date_sent)
        const formattedDate = moment(dateObj).format('MMMM DD YYYY HH:mm')
        const formattedDateString = formattedDate.toString()

        return {
          body: message.body,
          date: formattedDateString,
          to: message.to,
          from: message.from,
        }
      })

      return messages
    },
  })
}
