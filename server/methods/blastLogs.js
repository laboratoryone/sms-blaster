import { check } from 'meteor/check'
import twilio from 'twilio'

// import collections
import SubscribedBlasts from '../collections/ScheduledBlasts'
import Subscribed from '../collections/Subscribed'

// set up variables
const accountSid = Meteor.settings.private.twilio.account_sid
const authToken = Meteor.settings.private.twilio.auth_token
const client = twilio(accountSid, authToken)

export default function () {
  Meteor.methods({
    'getBlasts'() {
      // Make sure the user is logged in before inserting a task
      if (! this.userId) {
        throw new Meteor.Error('not-authorized')
      }

      const blasts = SubscribedBlasts.find().fetch()
      return blasts
    },
    'addBlast'(message, date) {
      check(message, String)
      check(date, String)

      // Make sure the user is logged in before inserting a task
      if (! this.userId) {
        throw new Meteor.Error('not-authorized')
      }

      SubscribedBlasts.insert({ message, date, status: 'pending' })
      return
    },
    'editBlast'(_id, message) {
      check(_id, String)
      check(message, String)

      // Make sure the user is logged in before inserting a task
      if (! this.userId) {
        throw new Meteor.Error('not-authorized')
      }

      SubscribedBlasts.update(_id, { $set: { message } })
      return
    },
    'sendBlast'(_id) {
      check(_id, String)

      // Make sure the user is logged in before inserting a task
      if (! this.userId) {
        throw new Meteor.Error('not-authorized')
      }

      // get blast
      const blast = SubscribedBlasts.findOne(_id, { status: 'pending' })
      const message = blast.message

      // get subscribers
      const subscribers = Subscribed.find({ subscribed: true }).fetch()

      // send blast
      for (var subscribed in subscribers) {
        const to = subscribers[subscribed].number

        client.messages.create({
          to: to,
          from: Meteor.settings.public.twilio.number,
          body: message,
        })
      }

      // update blast status
      SubscribedBlasts.update(_id, { $set: { status: 'sent' } })

      return
    },
  })
}
