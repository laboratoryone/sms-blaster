import blastLogs from './blastLogs'
import scheduler from './scheduler'
import sendSms from './sendSms'

export default function () {
  blastLogs()
  scheduler()
  sendSms()
}
