import twilio from 'twilio'
import { check } from 'meteor/check'

// set up variables
const accountSid = Meteor.settings.private.twilio.account_sid
const authToken = Meteor.settings.private.twilio.auth_token
const client = twilio(accountSid, authToken)

export default function () {
  Meteor.methods({
    'sendSms'(to, body) {
      check(to, String)
      check(body, String)

      // Make sure the user is logged in before inserting a task
      if (! this.userId) {
        throw new Meteor.Error('not-authorized')
      }

      client.messages.create({
        to,
        from: Meteor.settings.public.twilio.number,
        body,
      })
      return
    },
  })
}
