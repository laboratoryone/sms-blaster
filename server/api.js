import { Picker } from 'meteor/meteorhacks:picker'
import bodyParser from 'body-parser'
import twilio from 'twilio'

// import collections
import Subscribed from './collections/Subscribed'

export default function api() {
  // make it so we can read the body
  Picker.middleware(bodyParser.json())
  Picker.middleware(bodyParser.urlencoded({ extended: false }))

  const postRoutes = Picker.filter((req, res) => {
    return req.method === 'POST'
  })

  postRoutes.route('/incoming', (params, req, res) => {
    const from = req.body.From || ''

    if (from === '') {
      return
    }
    // Try to find a subscriber with the given phone number
    const inCollection = Subscribed.findOne({ number: from })

    // if no subscriber associated with this phone number
    if (inCollection === undefined) {
      Subscribed.insert({ number: from, subscribed: false })
      const response = new twilio.TwimlResponse()
      response.message(`Thanks for texting SMS Blaster! Text "join" to receive SMS Blasts`)
      res.writeHead(200, { 'Content-Type': 'text/xml' })
      res.end(response.toString())
      return
    }

    // handle their message
    let msg = req.body.Body || ''
    msg = msg.toLocaleLowerCase().trim()

    // Conditional logic to do different things based on the command from the user
    if (msg === 'subscribe' || msg === 'join') {
      // If the user has elected to subscribe for messages, flip the bit
      // and indicate that they have done so.
      Subscribed.update(inCollection, { $set: { subscribed: true } })

      const response = new twilio.TwimlResponse()
      response.message(`You are now subscribed for SMS Blasts. Text 'leave' to stop receiving messages`)
      res.writeHead(200, { 'Content-Type': 'text/xml' })
      res.end(response.toString())
    } else if (msg === 'unsubscribe' || msg === 'leave') {
      // If the user has elected to unsubscribe for messages, flip the bit
      // and indicate that they have done so.
      Subscribed.update(inCollection, { $set: { subscribed: false } })

      const response = new twilio.TwimlResponse()
      response.message(`You have unsubscribed. Text 'start' to start receiving SMS Blasts`)
      res.writeHead(200, { 'Content-Type': 'text/xml' })
      res.end(response.toString())
    } else {
      // If we don't recognize the command, text back with the list of
      // available commands
      const response = new twilio.TwimlResponse()
      response.message(`Sorry, we didn't understand that. Available commands are: join or leave`)
      res.writeHead(200, { 'Content-Type': 'text/xml' })
      res.end(response.toString())
    }

    return
  })
}
