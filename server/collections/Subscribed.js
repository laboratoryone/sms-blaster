import { Mongo } from 'meteor/mongo'

const Subscribed = new Mongo.Collection('subscribed')

export default Subscribed
