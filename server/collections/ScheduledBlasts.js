import { Mongo } from 'meteor/mongo'

const ScheduledBlasts = new Mongo.Collection('scheduledBlasts')

export default ScheduledBlasts
