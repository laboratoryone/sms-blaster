import api from './api'
import defaultUsers from './defaultUsers'
import methods from './methods/methods.js'
import publications from './publications/publications'

api()
defaultUsers()
methods()
publications()
