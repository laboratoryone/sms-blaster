// import dependencies
import { Meteor } from 'meteor/meteor'

// import collections
import ScheduledBlasts from '../collections/ScheduledBlasts'
import Subscribed from '../collections/Subscribed'

export default function () {
  Meteor.publish('scheduledBlasts', () => {
    return ScheduledBlasts.find()
  })

  Meteor.publish('subscribed', () => {
    return Subscribed.find()
  })
}
