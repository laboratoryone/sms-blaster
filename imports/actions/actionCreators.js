export function addBlast(message, date) {
  return {
    type: 'ADD_BLASTS',
    message,
    date,
  }
}

export function editBlast(_id, message) {
  return {
    type: 'EDIT_BLASTS',
    _id,
    message,
  }
}

// get sms logs
export function getLogs() {
  return {
    type: 'GET_LOGS',
  }
}

// get blast logs
export function getBlasts() {
  return {
    type: 'GET_BLASTS',
  }
}

export function toggleAddBlastEditor(active) {
  return {
    type: 'TOGGLE_ADD_BLAST_EDITOR',
    active,
  }
}

export function toggleBlastEditor(active, _id, message, index) {
  return {
    type: 'TOGGLE_BLAST_EDITOR',
    active,
    _id,
    message,
    index,
  }
}

export function toggleConfirmation(active, _id, message) {
  return {
    type: 'TOGGLE_CONFIRMATION',
    active,
    _id,
    message,
  }
}

export function toggleReplyEditor(active, body, to) {
  return {
    type: 'TOGGLE_REPLY_EDITOR',
    active,
    body,
    to,
  }
}

export function sendBlast(_id) {
  return {
    type: 'SEND_BLASTS',
    _id,
  }
}

export function replyToSms(to, message) {
  return {
    type: 'REPLY_TO_SMS',
    to,
    message,
  }
}
