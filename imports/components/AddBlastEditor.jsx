import React from 'react'
import { notify } from 'react-notify-toast';
import moment from 'moment'

const AddBlastEditor = React.createClass({
  handleAdd(e) {
    e.preventDefault();
    const { addBlastEditor } = this.props
    const message = this.refs.message.value
    const date = new Date()
    const formattedDate = moment(date).format("MMM DD YYYY HH:mm").toString()

    // how user error later
    if (message === '') {
      return
    }

    this.props.addBlast(message, formattedDate)
    this.refs.message.value = ''
    this.props.toggleAddBlastEditor(addBlastEditor.active)
    notify.show('Blast created', 'success')
  },
  render() {
    const { addBlastEditor } = this.props
    return (
      <div className={addBlastEditor.active ? "modal is-active" : "modal"}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Create a new blast</p>
            <a className="icon is-primary">
              <i className="fa fa-times" onClick={this.props.toggleAddBlastEditor.bind(null, addBlastEditor.active)} />
            </a>
            </header>
          <section className="modal-card-body">
            <label className="label">Message</label>
            <p className="control">
              <textarea className="textarea" ref="message" placeholder='A message you want to send to all your subscribers' required></textarea>
            </p>
          </section>
          <footer className="modal-card-foot">
            <span className="nav-right">
              <a className="button is-primary is-outlined" type="submit" onClick={this.handleAdd}>
                Save
              </a>
            </span>
          </footer>
        </div>
      </div>
    )
  }
})

export default AddBlastEditor
