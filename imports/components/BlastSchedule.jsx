import React from 'react'

// import components
import AddBlastEditor from './AddBlastEditor'
import BlastEditor from './BlastEditor'
import BlastRow from './BlastRow'
import Confirmation from './Confirmation'

const BlastSchedule = React.createClass({
  componentDidMount () {
    this.props.getBlasts()
  },
  render() {

    const { blasts, addBlastEditor } = this.props
    const isBlasts = blasts.length === 0 ? true : false

    return (
      <section className="section">
        <div className="container">
          <div className="control is-grouped">
            <h2 className="title">SMS Blasts</h2>

            <div className="nav-right">
              <div onClick={this.props.toggleAddBlastEditor.bind(null, addBlastEditor.active )}>
                <a className="is-primary">
                  Create a blast <p className="icon is-primary">
                  <i className="fa fa-plus" />
                </p>
                </a>
              </div>
            </div>
          </div>
          <table className="table">
            <thead>
              {isBlasts ? '' :
                <tr>
                  {Object.keys(blasts[0]).map((key, i) => {
                    if (i === 0) { return }
                    if (key === 'date') {
                      return <td key={i}>created on</td>
                    }
                    return <td key={i}>{key}</td>
                  })}
                  <td>Edit</td>
                  <td>Send</td>
               </tr>
              }
            </thead>
            <tbody>
              {isBlasts ? 'There are no scheduled blasts' : blasts.map((blast, i) => <BlastRow {...this.props} key={i} blast={blast} />)}
            </tbody>
          </table>
        </div>
        <BlastEditor {...this.props} />
        <AddBlastEditor {...this.props} />
        <Confirmation {...this.props} />
      </section>
    )
  }
})

export default BlastSchedule
