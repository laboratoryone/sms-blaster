import React from 'react'

const BlastRow = React.createClass({
  render() {
    const { blast, blastEditor, confirmation } = this.props
    const i = this.props.blasts.findIndex((b) => b._id === blast._id)
    const isPending = blast.status === 'pending' ? true : false
    return (
      <tr>
        <td>{blast.message}</td>
        <td>{blast.date}</td>
        <td>{blast.status}</td>
        { isPending ?
          <td>
            <a className="icon is-primary">
              <i className="fa fa-pencil-square-o" onClick={this.props.toggleBlastEditor.bind(null,  blastEditor.active, blast._id, blast.message, i)} />
            </a>
          </td>
        :
          <td></td>
        }
        { isPending ?
          <td>
            <a className="icon is-primary">
              <i className="fa fa-paper-plane" onClick={this.props.toggleConfirmation.bind(null,  confirmation.active, blast._id, blast.message)} />
            </a>
          </td>
        :
          <td></td>
        }
      </tr>
    )
  }
})

export default BlastRow
