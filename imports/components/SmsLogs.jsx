import React from 'react'

// import components
import SmsEditor from './SmsEditor'
import SmsRow from './SmsRow'

const SmsLogs = React.createClass({
  componentDidMount () {
    this.props.getLogs()
  },
  render() {

    const { logs } = this.props
    const isLogs = logs.length === 0 ? true : false

    return (
      <section className="section">
        <div className="container">

          <h2 className="title">SMS Log</h2>
          <h3 className="subtitle">Text {Meteor.settings.public.twilio.number} to test the workflow</h3>

          <table className="table">
            <thead>
              <tr>
                {isLogs ? '' :
                  Object.keys(logs[0]).map((key, i) => {
                    return <td key={i}>{key}</td>
                  }
                )}
                {isLogs ? '' : <td>Reply</td>}
              </tr>
            </thead>

            <tbody>
              {isLogs ?
                <tr>
                  <td>
                    <p>There are no SMS</p>
                  </td>
                </tr>
              :
                logs.map((log, i) => <SmsRow {...this.props} key={i} log={log} />)
              }
            </tbody>
          </table>

        </div>

        <SmsEditor {...this.props} />

      </section>
    )
  }
})

export default SmsLogs
