import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actionCreators from '../actions/actionCreators'
import Main from '../components/Main'

function mapStateToProps(state) {
  return {
    addBlastEditor: state.addBlastEditor,
    blasts: state.blasts,
    blastEditor: state.blastEditor,
    confirmation: state.confirmation,
    error: state.error,
    logs: state.logs,
    smsEditor: state.smsEditor,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch)
}

const App = connect(mapStateToProps, mapDispatchToProps)(Main)

export default App
