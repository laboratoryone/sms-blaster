import React from 'react'

const Footer = React.createClass({
  render() {
    return (
      <footer className="footer">
        <div className="container">
          <div className="content has-text-centered">
            <p>
             Created by <strong><a href="http://labone.tech">L A B O R A T O R Y O N E</a></strong>.
            </p>
          </div>
        </div>
      </footer>
    )
  }
})

export default Footer
