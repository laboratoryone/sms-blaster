import React from 'react'
import { notify } from 'react-notify-toast';

const SmsEditor = React.createClass({
  handleReply(e) {
    e.preventDefault()
    const { smsEditor } = this.props
    const message = this.refs.message.value

    if (message === '') {
      return
    }

    this.props.replyToSms(smsEditor.to, message)
    this.refs.message.value = ''
    this.props.toggleReplyEditor(smsEditor.active, '', '')
    notify.show('Reply sent', 'success')
  },
  render() {
    const { smsEditor } = this.props
    return (
      <div className={smsEditor.active ? "modal is-active" : "modal"}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Reply to SMS</p>
            <a className="icon is-primary">
              <i className="fa fa-times" onClick={this.props.toggleReplyEditor.bind(null, smsEditor.active, '', '')} />
            </a>
            </header>
          <section className="modal-card-body">
            <div className="control is-horizontal">
              <div className="control-label">
                <label className="label">To</label>
              </div>
              <input className="input" type="text" ref="To" value={smsEditor.to} required />
            </div>
            <label className="label">Message</label>
            <p className="control">
              <textarea className="textarea" ref="message" placeholder={smsEditor.body} required></textarea>
            </p>
          </section>
          <footer className="modal-card-foot">
            <span className="nav-right">
              <a className="button is-primary is-outlined" type="submit" onClick={this.handleReply}>
                Send
              </a>
            </span>
          </footer>
        </div>
      </div>
    )
  }
})

export default SmsEditor
