import React from 'react'
import { notify } from 'react-notify-toast';

const BlastEditor = React.createClass({
  handleEdit(e) {
    e.preventDefault()
    const { blastEditor } = this.props
    const message = this.refs.message.value

    // how user error later
    if (message === '') {
      return
    }

    this.props.editBlast(blastEditor._id, message)
    this.refs.message.value = ''
    this.props.toggleBlastEditor(blastEditor.active, '', '', '', null)
    notify.show('Blast edited', 'success')
  },
  render() {
    const { blastEditor } = this.props
    return (
      <div className={blastEditor.active ? "modal is-active" : "modal"}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Edit Scheduled Blast</p>
            <a className="icon is-primary">
              <i className="fa fa-times" onClick={this.props.toggleBlastEditor.bind(null, blastEditor.active, '', '', '', null)} />
            </a>
            </header>
          <section className="modal-card-body">
            <label className="label">Message</label>
            <p className="control">
              <textarea className="textarea" ref="message" placeholder={blastEditor.message} required></textarea>
            </p>
          </section>
          <footer className="modal-card-foot">
            <span className="nav-right">
              <a className="button is-primary is-outlined" type="submit" onClick={this.handleEdit}>
                Save
              </a>
            </span>
          </footer>
        </div>
      </div>
    )
  }
})

export default BlastEditor
