import React from 'react'
import { Link } from 'react-router'
import Notifications from 'react-notify-toast'

// import components
import Footer from './Footer'

const Main = React.createClass({
  handleLogin(e) {
    e.preventDefault()
    const username = this.refs.username.value
    const password = this.refs.password.value

    // would be better if we used state to do this but whatever
    Meteor.loginWithPassword(username, password, () => {
      document.location.reload(true)
    })
    this.refs.loginForm.reset()
  },
  handleLogout(e) {
    e.preventDefault()
    // would be better if we used state to do this but whatever
    Meteor.logout(() => {
      document.location.reload(true)
    })
  },
  render () {
    return (
      <div>
        <section className="section">
          <Notifications />
          <div className="header">
            <div className="nav">
              <div className="container">
                <div className="nav-left">
                  <Link className="nav-item title" to="/">
                    SMS Blaster
                  </Link>
                </div>
                <div className="nav-item">
                  <Link className={this.props.location.pathname === '/' ? 'nav-item is-active' : 'nav-item'} to={'/'}>
                    Blast Schedule
                  </Link>
                  <Link className={this.props.location.pathname === '/smslogs' ? 'nav-item is-active' : 'nav-item'} to={'/smslogs'}>
                    SMS Logs
                  </Link>
                  <div className="nav-item">
                    {Meteor.userId() ?
                      <a className="button is-small is-primary is-outlined" onClick={this.handleLogout}>Logout</a>
                    :
                      <form className="control has-addons" ref="loginForm" onSubmit={this.handleLogin}>
                        <input className="input is-small" type="text" ref="username" placeholder="username" />
                        <input className="input is-small" type="password" ref="password" placeholder="password" />
                        <button className="button is-small is-primary is-outlined" type="submit">Login</button>
                      </form>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
            {Meteor.userId() ?
              React.cloneElement(this.props.children, this.props)
              :
              <section className="section is-large has-text-centered">
                <h2 className="title">Please sign in</h2>
              </section>
            }
        </section>
        <Footer />
      </div>
    )
  }
})

export default Main
