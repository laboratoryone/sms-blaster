import React from 'react'
import { notify } from 'react-notify-toast';

const BlastEditor = React.createClass({
  handleConfirmation(e) {
    e.preventDefault()
    const { confirmation } = this.props
    this.props.sendBlast(confirmation._id)
    this.props.toggleConfirmation(confirmation.active, '', '')
    notify.show('Blast sent', 'success')
  },
  render() {
    const { confirmation } = this.props
    return (
      <div className={confirmation.active ? "modal is-active" : "modal"}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Are you sure you want to send this blast?</p>
            <a className="icon is-primary">
              <i className="fa fa-times" onClick={this.props.toggleConfirmation.bind(null, confirmation.active, '', '')} />
            </a>
            </header>
          <section className="modal-card-body">
            <p className="has-centered-text">{confirmation.message}</p>
          </section>
          <footer className="modal-card-foot">
            <a className="button is-danger" type="submit" onClick={this.props.toggleConfirmation.bind(null, confirmation.active, '', '')}>
              Don't Send
            </a>
            <a className="button is-primary is-outlined" type="submit" onClick={this.handleConfirmation}>
              Send
            </a>
          </footer>
        </div>
      </div>
    )
  }
})

export default BlastEditor
