import React from 'react'

const SmsRow = React.createClass({
  render() {
    const { log, smsEditor } = this.props
    return (
      <tr>
        <td>{log.body}</td>
        <td>{log.date}</td>
        <td>{log.to}</td>
        <td>{log.from}</td>
        {log.from !== Meteor.settings.public.twilio.number ?
          <td>
            <a className="icon is-primary">
              <i className="fa fa-reply" onClick={this.props.toggleReplyEditor.bind(null, smsEditor.active, log.body, log.from)} />
            </a>
          </td>
        :
          <td></td>
        }
      </tr>
    )
  }
})

export default SmsRow
