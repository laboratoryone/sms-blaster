import { takeEvery } from 'redux-saga'
import { put, cps } from 'redux-saga/effects'

function* getBlasts() {
  try {
    const blastLogs = yield cps(Meteor.call, 'getBlasts')
    yield put({ type: 'POPULATE_BLASTS', blasts: blastLogs })
  } catch (e) {
    throw new Error(e)
  }
}

export default function* blasts() {
  yield* takeEvery('GET_BLASTS', getBlasts)
}
