import { takeEvery } from 'redux-saga'
import { put, cps } from 'redux-saga/effects'

function* getLogs() {
  try {
    const smsLogs = yield cps(Meteor.call, 'getMessages')
    yield put({ type: 'POPULATE_LOGS', logs: smsLogs })
  } catch (e) {
    throw new Error(e)
  }
}

export default function* logs() {
  yield* takeEvery('GET_LOGS', getLogs)
}
