import { takeEvery } from 'redux-saga'
import { put, cps } from 'redux-saga/effects'

function* reply(action) {
  try {
    yield cps(Meteor.call, 'sendSms', action.to, action.message)

    const smsLogs = yield cps(Meteor.call, 'getMessages')
    yield put({ type: 'POPULATE_LOGS', logs: smsLogs })

  } catch (e) {
    throw new Error(e)
  }
}

export default function* replyToSms() {
  yield* takeEvery('REPLY_TO_SMS', reply)
}
