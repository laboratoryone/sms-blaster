import { fork } from 'redux-saga/effects'

import addBlast from './addBlast'
import blasts from './blasts'
import editBlast from './editBlast'
import logs from './logs'
import sendBlast from './sendBlast'
import replyToSms from './replyToSms'

export default function* rootSaga() {
  yield [
    fork(addBlast),
    fork(blasts),
    fork(editBlast),
    fork(logs),
    fork(sendBlast),
    fork(replyToSms),
  ]
}
