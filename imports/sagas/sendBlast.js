import { takeEvery } from 'redux-saga'
import { put, cps } from 'redux-saga/effects'

function* send(action) {
  try {
    yield cps(Meteor.call, 'sendBlast', action._id)

    const blastLogs = yield cps(Meteor.call, 'getBlasts')

    yield put({ type: 'POPULATE_BLASTS', blasts: blastLogs })
  } catch (e) {
    throw new Error(e)
  }
}

export default function* sendBlast() {
  yield* takeEvery('SEND_BLASTS', send)
}
