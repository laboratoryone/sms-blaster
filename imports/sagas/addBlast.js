import { takeEvery } from 'redux-saga'
import { put, cps } from 'redux-saga/effects'

function* add(action) {
  try {
    yield cps(Meteor.call, 'addBlast', action.message, action.date)

    const blastLogs = yield cps(Meteor.call, 'getBlasts')

    yield put({ type: 'POPULATE_BLASTS', blasts: blastLogs })
  } catch (e) {
    throw new Error(e)
  }
}

export default function* addBlast() {
  yield* takeEvery('ADD_BLASTS', add)
}
