import { takeEvery } from 'redux-saga'
import { put, cps } from 'redux-saga/effects'

function* edit(action) {
  try {
    yield cps(Meteor.call, 'editBlast', action._id, action.message)

    const blastLogs = yield cps(Meteor.call, 'getBlasts')

    yield put({ type: 'POPULATE_BLASTS', blasts: blastLogs })
  } catch (e) {
    throw new Error(e)
  }
}

export default function* editBlast() {
  yield* takeEvery('EDIT_BLASTS', edit)
}
