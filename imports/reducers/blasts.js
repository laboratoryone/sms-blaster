export default function blasts(state = [], action) {
  switch (action.type) {
    case 'POPULATE_BLASTS':
      return action.blasts

    default:
      return state
  }
}
