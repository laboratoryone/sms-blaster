export default function smsEditor(state = [], action) {
  switch (action.type) {
    case 'TOGGLE_REPLY_EDITOR':
      return {
        active: !action.active,
        body: action.body,
        to: action.to,
      }
    default:
      return state
  }
}
