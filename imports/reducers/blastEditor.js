export default function blastEditor(state = [], action) {
  switch (action.type) {
    case 'TOGGLE_BLAST_EDITOR':
      return {
        active: !action.active,
        _id: action._id,
        message: action.message,
        index: action.index,
      }
    default:
      return state
  }
}
