export default function confirmation(state = [], action) {
  switch (action.type) {
    case 'TOGGLE_CONFIRMATION':
      return {
        active: !action.active,
        _id: action._id,
        message: action.message,
      }
    default:
      return state
  }
}
