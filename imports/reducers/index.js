import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import addBlastEditor from './addBlastEditor'
import blasts from './blasts'
import blastEditor from './blastEditor'
import confirmation from './confirmation'
import logs from './logs'
import smsEditor from './smsEditor'

const rootReducer = combineReducers({
  addBlastEditor,
  blasts,
  blastEditor,
  confirmation,
  logs,
  routing: routerReducer,
  smsEditor,
})

export default rootReducer
