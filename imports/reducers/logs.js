export default function logs(state = [], action) {
  switch (action.type) {
    case 'POPULATE_LOGS':
      return action.logs

    default:
      return state
  }
}
