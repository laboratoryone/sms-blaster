export default function addBlastEditor(state = [], action) {
  switch (action.type) {
    case 'TOGGLE_ADD_BLAST_EDITOR':
      return {
        active: !action.active,
      }
    default:
      return state
  }
}
