# Nightjar SMS Blaster
Peter Chau
Start Date: May 24, 2016

## Getting Start
1. Clone this repo with ```git clone https://northwest69@bitbucket.org/laboratoryone/sms-blaster-app.git```
2. Install Meteor with ```curl https://install.meteor.com/ | sh```
3. Install NPM deps with ```npm install```
4. Configure your settings.json
5. Run ```npm start``` to initialize npm container (remove port config in the command if you're not on c9)
6. Start with ```npm start```

## Dev Details
1. Lint with ```npm run lint```
2. Test with ```npm test``` or ```npm run-script test:watch```
3. Reset the database with ```npm run reset```

## Additional Information
- This project uses Sass and autoprefixer. Files are compiled by Meteor on start
- Reducers are responsible for handling state transitions between actions.
- Sagas are responsible for orchestrating complex/asynchronous operations.